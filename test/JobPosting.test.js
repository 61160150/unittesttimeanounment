const assert = require('assert')
const { checkEnableTime } = require('../JobPosting')
describe('JobPosting', function () {
  it('เวลาสมัครอยู่หลังเวลา เริ่มต้นและสิ้นสุด', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 7)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('เวลาสมัครอยู่ระหว่าง เริ่มต้นและสิ้นสุด return true', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 3)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('เวลาสมัครเป็นวันเดียวกับวันที่เริ่มต้น return true', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 31)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('เวลาสมัครเป็นวันเดียวกับวันที่สิ้นสุด return true', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 2, 5)
    const expectedResult = true
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
  it('เวลาสมัครอยู่ก่อนเวลา เริ่มต้นและสิ้นสุด return false', function () {
    // Arrage
    const startTime = new Date(2021, 1, 31)
    const endTime = new Date(2021, 2, 5)
    const today = new Date(2021, 1, 30)
    const expectedResult = false
    // Act
    const actualResult = checkEnableTime(startTime, endTime, today)
    // Assert
    assert.strictEqual(actualResult, expectedResult)
  })
})
